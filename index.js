// Two Rooms and a Boom online timer

/*
Either click a new game button, or enter an id manually
Once in the game, have the options to email, sms, or NFC

Clients will be sent a start time and duration, then the client will need to figure out how much it lags behind based on actual lag, and clock differences.


*/

// Requires
var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http),
	colors = require('colors'),
	shortid = require('shortid'),
	round = 0;

// Settings
var port = 80;

// Setup Express
app.use('/static', express.static('static'));
app.get('/', function(req, res) {
	res.send('INDEX <a href="'+shortid.generate()+'">GAME</a>');
});
app.get('/:gameid', function(req, res) {
	res.sendfile('timer.html');
});

// Start Express
http.listen(port, function() {
	console.log('Started on port '+port);
});

io.on('connection', function(socket) {
	var ip = socket.request.connection.remoteAddress;

	// Start Timers
	socket.on('start', function() {
		io.sockets.emit('timer',round);
		round = (round+1) % 5;
	})
});